## DevOps Course Project - EPITA

Welcome to our DevOps course project at EPITA. We, Simon DEFOORT, Albert LAWANDOS and Gabriel QUINT, have collaborated to create this simple yet functional web application for task management. Our project showcases the fundamental practices and tools used in DevOps, including Docker, GitLab CI/CD, and automated testing.

This project serves as a learning platform for us to understand and apply the concepts of Continuous Integration, Continuous Deployment, Containerization, and Code Quality Maintenance in a real-world scenario. We hope that our learning journey can also help other students or anyone interested in learning about DevOps practices.

## Project Structure

The project consists of the following main files:

- `app.py`: This is the main Flask application script which controls the routes and logic of the application.
- `Dockerfile`: This file specifies how to build the Docker images of our application. It has two stages: build and production.
- `.gitlab-ci.yml`: This file specifies the CI/CD pipeline stages and tasks to run whenever a change is pushed to the Git repository.
- `Tests/test_create_and_delete.py`: This is a test file which tests the creation and deletion of tasks in our application.

## How to Run Locally

1. *Clone the repository*:
   Use the following command to clone the repository to your local machine:
   
   git clone <repository_url>
   
2. *Navigate to the project directory*:
   After cloning, navigate to the project directory:
   
   cd <project_directory>
   
3. *Start the application*:
   Use the following command to start the application locally:
   
   python3 app.py
   
   Once you've started the application, you should be able to access it on `http://localhost:5000`.

## Docker Commands

This project utilizes Docker to build and run the application. Here are the necessary commands:

1. *Build the application images*:
   To build the Docker images for the build and production stages, use the following commands:
   
   docker build --target build -t my-app-build .
   docker build --target production -t my-app-production .
   
   These commands build the Docker images and tag them as `my-app-build` and `my-app-production` respectively.

2. *Run the application containers*:
   To run the Docker containers for the build and production stages, use the following commands:
   
   docker run -p 5000:5000 my-app-build
   
	  This build launch the app in development mode so you can see all the logs.
      
   docker run -p 5000:5000 my-app-production
      
      This build launch the app in production mode.

   These commands run the Docker containers and expose them on port `5000`.

## GitLab CI/CD

This project has a continuous integration (CI) and continuous deployment (CD) pipeline configured in the `.gitlab-ci.yml` file. The stages of the pipeline include:

1. *Build*: This stage builds the Docker images for the application.
2. *Mongo_tests*: This stage tests the task creation and deletion functionality in the application.
3. *App_tests*: This stage tests the home route of the application.
4. *Code_quality*: This stage checks the quality of the code using Flake8, a Python linter.
5. *Security*: This stage runs a security scan of the code using the `safety` Python package.
6. *Performance*: This stage performs performance testing using JMeter.
7. *Deploy*: This stage deploys the application to the production server using SSH and Docker.

Each stage is defined as a separate job in the `.gitlab-ci.yml` file with its own `script` section specifying the tasks to be performed. The `before_script` section in each job specifies tasks to be performed before the main `script`.

Please note that the pipeline will run automatically whenever you push a change to the repository.

---

For any additional help or clarification, please refer to the official documentation.