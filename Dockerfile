# ----- BUILD STAGE -----
FROM python:3.10-slim-buster as build

# Set the working directory in the container to /app
WORKDIR /app

# Add the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Make port 5000 available to the world outside this container
EXPOSE 5000

# Run app.py when the container launches
CMD ["python", "app.py"]

# ----- PRODUCTION STAGE -----
# Starts a new stage from scratch
FROM python:3.10-slim-buster as production

# Copy the app from the build stage
COPY --from=build /app /app

# Set the working directory in the container to /app
WORKDIR /app

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Make port 5000 available to the world outside this container
EXPOSE 5000

#Run with Gunicorn
CMD ["gunicorn", "-b", ":5000", "app:app"]