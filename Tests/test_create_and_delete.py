from flask import Flask, render_template, request, url_for, redirect
from pymongo import MongoClient
from bson.objectid import ObjectId
import os

app = Flask(__name__)

uri = os.getenv("MONGODB_URI")

client = MongoClient(uri)
db = client.flask_db
todos = db.todos


@app.route('/', methods=('GET', 'POST'))
def index():
    if request.method=='POST':
        content = request.form['content']
        degree = request.form['degree']
        todos.insert_one({'content': content, 'degree': degree})
        return redirect(url_for('index'))

    all_todos = todos.find()
    return render_template('index.html', todos=all_todos)

@app.route('/<id>/delete/', methods=['POST'])
def delete(id):
    todos.delete_one({"_id": ObjectId(id)})
    return redirect(url_for('index'))


def test_create_and_delete():
    with app.test_client() as client:
        # First, add a dummy todo to delete
        todos.insert_one({'content': 'Dummy Todo', 'degree': 'Important'})
        # Get the inserted todo ID
        inserted_todo = todos.find_one({'content': 'Dummy Todo'})
        todo_id = str(inserted_todo['_id'])
        
        # Send a POST request to delete the todo
        response = client.post(f'/{todo_id}/delete/')
        
        # Check if the response redirects to the index page
        assert response.status_code == 302
        assert response.location == 'http://localhost/'

        # Check if the todo is successfully deleted
        deleted_todo = todos.find_one({'_id': ObjectId(todo_id)})
        assert deleted_todo is None
